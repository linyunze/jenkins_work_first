package com.example.jenkins_work_first;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsWorkFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(JenkinsWorkFirstApplication.class, args);
    }

}
